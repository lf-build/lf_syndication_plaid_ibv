﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.PlaidIBV {
    public class PlaidConfiguration : IPlaidConfiguration, IDependencyConfiguration {
        public string BaseUrl { get; set; }
        public string ClientId { get; set; }
        public string Secret { get; set; }
        public bool UseSimulation { get; set; }
        public string SimulationURL { get; set; }
        public string PublicKey { get; set; }
        public bool IsIdentityProduct { get; set; }

        public string ClientName { get; set; }

        public string ApiVersion { get; set; }

        public string Env { get; set; }

        public string Key { get; set; }

        public List<string> Product { get; set; }

        public bool SelectAccount { get; set; }

        public string WebhookUrl { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }

    }
}