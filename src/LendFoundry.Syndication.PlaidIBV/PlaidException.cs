﻿using System;
using System.Runtime.Serialization;
namespace LendFoundry.Syndication.PlaidIBV
{
    [Serializable]
    public class PlaidException : Exception
    {
        public string ErrorCode { get; set; }

        public PlaidException()
        {
        }

        public PlaidException(string message) : this(null, message, null)
        {
        }

        public PlaidException(string errorCode, string message) : this(errorCode, message, null)
        {
        }

        public PlaidException(string errorCode, string message, Exception innerException) : base(message, innerException)
        {
            ErrorCode = errorCode;
        }

        public PlaidException(string message, Exception innerException) : this(null, message, innerException)
        {
        }

        protected PlaidException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
