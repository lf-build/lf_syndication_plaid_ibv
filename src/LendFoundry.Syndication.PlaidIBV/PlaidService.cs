﻿using System.Threading.Tasks;
using System;
namespace LendFoundry.Syndication.PlaidIBV
{
    /// <summary>
    /// PlaidService
    /// </summary>
    public class PlaidService : IPlaidService
    {
        #region constructors

        /// <summary>
        /// PlaidService
        /// </summary>
        /// <param name="plaidServiceProxy"></param>
        public PlaidService(IPlaidProxy plaidServiceProxy)
        {
            if (plaidServiceProxy == null)
                throw new ArgumentNullException(nameof(plaidServiceProxy));

            PlaidServiceProxy = plaidServiceProxy;
        }

        #endregion

        #region variable declaration

        /// <summary>
        /// Gets PlaidServiceProxy
        /// </summary>
        private IPlaidProxy PlaidServiceProxy { get; }

        #endregion

        #region Plaid Version-2

        /// <summary>
        /// GetAccountsDetails
        /// </summary>
        /// <param name="bankLinkRequest"></param>
        /// <returns></returns>
        public async Task<IPlaidResponse> GetAccountsDetails(IBankLinkRequest bankLinkRequest, string insId = null)
        {
            if (string.IsNullOrWhiteSpace(bankLinkRequest.PublicToken))
                throw new ArgumentException("PublicToken is null or whitespace", nameof(bankLinkRequest.PublicToken));

            //// Exchange token for getting access token
            var exchangeResponse = await PlaidServiceProxy.GetAccessTokenAsync(bankLinkRequest.PublicToken);

            var response = await PlaidServiceProxy.GetAccountsWithNumbersAsync(exchangeResponse.AccessToken, insId);

            if (!response.HasError)
            {
                var institutionResponse = await PlaidServiceProxy.InstitutionsGetByIdAsync(response.Item.Institution_Id);
                response.InstitutionName = institutionResponse.Institution.InstitutionName;
                response.Item.Item_Id = exchangeResponse.ItemId;//For Simulation update item_id for webhook
            }

            if (!response.HasError)
            {
                var identityResponse = await PlaidServiceProxy.GetIdentityAsync(exchangeResponse.AccessToken);
                if (identityResponse != null)
                {
                    response.Owners = identityResponse.identity.names;
                }
            }

            return new PlaidResponse(response, exchangeResponse.AccessToken);
        }

        /// <summary>
        /// AccountSync
        /// </summary>
        /// <param name="bankLinkRequest"></param>
        /// <returns></returns>
        public async Task<IPlaidResponse> AccountSync(IBankLinkRequest bankLinkRequest)
        {
            if (string.IsNullOrWhiteSpace(bankLinkRequest.AccessToken))
                throw new ArgumentException("AccessToken is null or whitespace", nameof(bankLinkRequest.AccessToken));

            var response = await PlaidServiceProxy.GetAccountsWithNumbersAsync(bankLinkRequest.AccessToken);

            return new PlaidResponse(response, bankLinkRequest.AccessToken);
        }

        /// <summary>
        /// DeleteItemConnection
        /// </summary>
        /// <param name="bankLinkRequest"></param>
        /// <returns></returns>
        public async Task<IPlaidResponseItemDelete> DeleteItemConnection(IBankLinkRequest bankLinkRequest)
        {
            if (string.IsNullOrWhiteSpace(bankLinkRequest.AccessToken))
                throw new ArgumentException("Argument is null or whitespace", nameof(bankLinkRequest.AccessToken));

            var response = await PlaidServiceProxy.ItemDeleteAsync(bankLinkRequest.AccessToken);

            return response;
        }

        /// <summary>
        /// CreatePublicTokenForRefresh
        /// </summary>
        /// <param name="bankLinkRequest"></param>
        /// <returns></returns>
        public async Task<IPlaidCreatePublicTokenResponse> CreatePublicTokenForRefresh(IBankLinkRequest bankLinkRequest)
        {
            if (string.IsNullOrWhiteSpace(bankLinkRequest.AccessToken))
                throw new ArgumentException("Argument is null or whitespace", nameof(bankLinkRequest.AccessToken));

            var response = await PlaidServiceProxy.GetPublicTokenAsync(bankLinkRequest.AccessToken);
            return response;
        }

        /// <summary>
        /// AddBank
        /// </summary>
        /// <param name="bankLinkRequest"></param>
        /// <returns></returns>
        public async Task<IPlaidResponse> AddBank(IBankLinkRequest bankLinkRequest)
        {
            if (string.IsNullOrWhiteSpace(bankLinkRequest.AccessToken))
                throw new ArgumentException("Argument is null or whitespace", nameof(bankLinkRequest.AccessToken));

            var response = await PlaidServiceProxy.GetAccountsWithNumbersAsync(bankLinkRequest.AccessToken);

            if (!response.HasError)
            {
                var institutionResponse = await PlaidServiceProxy.InstitutionsGetByIdAsync(response.Item.Institution_Id);
                response.InstitutionName = institutionResponse.Institution.InstitutionName;
            }

            if (!response.HasError)
            {
                var identityResponse = await PlaidServiceProxy.GetIdentityAsync(bankLinkRequest.AccessToken);
                if (identityResponse != null)
                {
                    response.Owners = identityResponse.identity.names;
                }
            }

            return new PlaidResponse(response, bankLinkRequest.AccessToken);
        }

        /// <summary>
        /// GetTransactions
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<IPlaidResponse> GetTransactions(ITransactionRequest request, string institutionId)
        {
            if (string.IsNullOrWhiteSpace(request.AccessToken))
                throw new ArgumentException("Argument is null or whitespace", nameof(request.AccessToken));

            var response = await PlaidServiceProxy.PullTransactionAsync(request, institutionId);
            return new PlaidResponse(response, request.AccessToken);
        }
        #endregion

        #region private methods

        #endregion
    }
}