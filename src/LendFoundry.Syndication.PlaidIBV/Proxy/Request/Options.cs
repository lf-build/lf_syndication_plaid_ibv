﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    /// <summary>
    /// Options
    /// </summary>
    public class Options
    {
        [JsonProperty("webhook")]
        public string Webhook { get; set; }


    }

    public class OptionsForTransaction
    {
        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("offset")]
        public int Offset { get; set; }
    }

    public class SearchOptions
    {
        [JsonProperty("include_display_data")]
        public bool IncludeDisplayData { get; set; }
    }
}
