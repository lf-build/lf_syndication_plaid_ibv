﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{

    public interface IPlaidRequestCommon
    {
        string Client_Id { get; set; }

        string Secret { get; set; }

        string Access_Token { get; set; }
    }

    public class PlaidRequestCommon : IPlaidRequestCommon
    {
        [JsonProperty("client_id")]
        public string Client_Id { get; set; }

        [JsonProperty("secret")]
        public string Secret { get; set; }

        [JsonProperty("access_token")]
        public string Access_Token { get; set; }
    }
}
