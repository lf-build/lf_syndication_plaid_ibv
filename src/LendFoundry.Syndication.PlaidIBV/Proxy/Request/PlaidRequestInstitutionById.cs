﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class PlaidRequestInstitutionById : IPlaidRequestInstitutionById
    {
        [JsonProperty("institution_id")]
        public string InstitutionId { get; set; }

        [JsonProperty("public_key")]
        public string PublicKey { get; set; }

        //[JsonProperty("options")]
        //public SearchOptions Options { get; set; }
    }
}
