﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class PlaidRequestPublicTokenExchange
    {
        [JsonProperty("client_id")]
        public string Client_Id { get; set; }

        [JsonProperty("secret")]
        public string Secret { get; set; }

        [JsonProperty("public_token")]
        public string Public_Token { get; set; }
    }
}
