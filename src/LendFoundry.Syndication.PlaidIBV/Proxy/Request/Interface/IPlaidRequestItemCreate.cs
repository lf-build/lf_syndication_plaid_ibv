﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidRequestItemCreate
    {
        Credential Credentials { get; set; }

        string Institution_Id { get; set; }

        string Public_Key { get; set; }

        Options Options { get; set; }

        string[] Initial_Products { get; set; }
    }
}
