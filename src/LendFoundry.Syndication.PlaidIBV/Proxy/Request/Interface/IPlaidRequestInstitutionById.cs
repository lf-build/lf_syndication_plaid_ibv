﻿namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidRequestInstitutionById
    {
        string InstitutionId { get; set; }

        string PublicKey { get; set; }

        //SearchOptions Options { get; set; }
    }
}
