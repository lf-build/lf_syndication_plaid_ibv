﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidRequestTransaction
    {
        string ClientId { get; set; }

        string Secret { get; set; }

        string AccessToken { get; set; }

        string StartDate { get; set; }

        string EndDate { get; set; }

        OptionsForTransaction Options { get; set; }
    }
}
