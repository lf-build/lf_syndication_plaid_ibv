﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class PlaidRequestItemUpdate : IPlaidRequestItemUpdate
    {
        [JsonProperty("credentials")]
        public Credential Credentials { get; set; }

        [JsonProperty("public_token")]
        public string  PublicToken{ get; set; }

        [JsonProperty("public_key")]
        public string Public_Key { get; set; }
    }
}



