﻿using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidProxy
    {
        #region Plaid Sandbox New

        Task<IPlaidResponseAccounts> PullTransactionAsync(ITransactionRequest transactionRequestData, string institutionId);
        //Task<IPlaidResponseTransaction> PullTransactionAsync(ITransactionRequest transactionRequestData);
        Task<IPlaidResponsePublicTokenExchange> GetAccessTokenAsync(string public_token);

        Task<IPlaidResponseAccounts> GetAccountAsync(string accessToken);

        Task<IPlaidResponseAccounts> GetAccountsWithNumbersAsync(string accessToken, string insId = null);
        Task<IPlaidResponseItemDelete> ItemDeleteAsync(string accessToken);

        Task<IPlaidCreatePublicTokenResponse> GetPublicTokenAsync(string accessToken);

        Task<IPlaidResponseInstitution> InstitutionsGetByIdAsync(string institutionId);

        Task<IPlaidResponseIdentity> GetIdentityAsync(string accessToken);
        #endregion

        #region Plaid Legacy

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="publicToken"></param>
        ///// <returns></returns>
        //Task<IExchangeTokenResponse> ExchangeToken(string publicToken);
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="incomeRequest"></param>
        ///// <returns></returns>
        //Task<IIncomeResponse> GetIncome(IIncomeRequest incomeRequest);
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="transactionRequest"></param>
        ///// <returns></returns>
        //Task<ITransactionResponse> GetTransaction(ITransactionRequest transactionRequest);
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="institutionsRequest"></param>
        ///// <returns></returns>
        //Task<IInstitutionsResponse> GetInstitutions(IInstitutionsRequest institutionsRequest);
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="institutionSearchRequest"></param>
        ///// <returns></returns>
        //Task<IInstitutionSearchResponse> SearchInstitutions(IInstitutionSearchRequest institutionSearchRequest);

        #endregion
    }
}
