﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidResponseInstitution
    {
        Institution Institution { get; set; }

        string RequestId { get; set; }

        PlaidResponseError Error { get; set; }

        bool HasError { get; set; }

    }
}
