﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidResponseIdentity
    {
        List<Accounts> accounts { get; set; }

        Identity identity { get; set; }

        Item item { get; set; }

        string request_id { get; set; }

         PlaidResponseError Error { get; set; }

         bool HasError { get; set; }
    }
}
