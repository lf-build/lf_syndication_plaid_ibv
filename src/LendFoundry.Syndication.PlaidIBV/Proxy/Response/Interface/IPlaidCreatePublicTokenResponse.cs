﻿namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidCreatePublicTokenResponse
    {
        string PublicToken { get; set; }
        string RequestId { get; set; }
    }
}