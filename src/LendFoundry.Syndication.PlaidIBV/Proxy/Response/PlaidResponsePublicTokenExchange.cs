﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidResponsePublicTokenExchange
    {
         string AccessToken { get; set; }

         string ItemId { get; set; }

         string RequestId { get; set; }
    }

    public class PlaidResponsePublicTokenExchange : IPlaidResponsePublicTokenExchange
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("item_id")]
        public string ItemId { get; set; }

        [JsonProperty("request_id")]
        public string RequestId { get; set; }
    }
}
