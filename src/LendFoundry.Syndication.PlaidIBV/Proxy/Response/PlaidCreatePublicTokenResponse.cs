﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class PlaidCreatePublicTokenResponse : IPlaidCreatePublicTokenResponse
    {
        [JsonProperty("public_token")]
        public string PublicToken { get; set; }
        
        [JsonProperty("request_id")]
        public string RequestId { get; set; }
    }
}
