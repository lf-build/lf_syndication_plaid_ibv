﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class PlaidResponseInstitution : IPlaidResponseInstitution
    {
        [JsonProperty("institution")]
        public Institution Institution { get; set; }

        [JsonProperty("request_id")]
        public string RequestId { get; set; }

        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }

        [JsonProperty("has_error")]
        public bool HasError { get; set; }
    }

    public class Institution
    {
        [JsonProperty("credentials")]
        public List<Credential> Credentials { get; set; }

        [JsonProperty("has_mfa")]
        public bool HasMFA { get; set; }

        [JsonProperty("institution_id")]
        public string InstitutionId { get; set; }

        [JsonProperty("mfa")]
        public string[] Mfa { get; set; }

        [JsonProperty("name")]
        public string InstitutionName { get; set; }

        [JsonProperty("products")]
        public string[] Products { get; set; }
    }
}




