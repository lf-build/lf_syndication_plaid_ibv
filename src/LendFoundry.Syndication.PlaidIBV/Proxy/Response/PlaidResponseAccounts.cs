﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidResponseAccounts
    {
        string PlaidItemId { get; set; }

        string InstitutionId { get; set; }

        string InstitutionName { get; set; }

        string Name { get; set; }

        string Logo { get; set; }

        string[] Owners { get; set; }
        List<Accounts> Accounts { get; set; }

        List<Number> Numbers { get; set; }
        bool HasError { get; set; }

        Item Item { get; set; } 

        List<Transaction> Transactions { get; set; }

        PlaidResponseError Error { get; set; }
    }

    public class PlaidResponseAccounts : IPlaidResponseAccounts
    {
        [JsonProperty("plaid_item_id")]
        public string PlaidItemId { get; set; }

        [JsonProperty("institutionId")]
        public string InstitutionId { get; set; }

        [JsonProperty("institutionName")]
        public string InstitutionName { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }

        [JsonProperty("owners")]
        public string[] Owners { get; set; }

        [JsonProperty("accounts")]
        public List<Accounts> Accounts { get; set; }

        [JsonProperty("numbers")]
        public List<Number> Numbers { get; set; }

        [JsonProperty("item")]
        public Item Item { get; set; } 

        [JsonProperty("has_error")]
        public bool HasError { get; set; }

        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }

        [JsonProperty("transactions")]
        public List<Transaction> Transactions { get; set; }
    }
}