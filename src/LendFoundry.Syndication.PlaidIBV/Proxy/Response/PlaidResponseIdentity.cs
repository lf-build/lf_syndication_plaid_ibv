﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class PlaidResponseIdentity : IPlaidResponseIdentity
    {
        [JsonProperty("Accounts")]
        public List<Accounts> accounts { get; set; }

        [JsonProperty("identity")]
        public Identity identity { get; set; }

        [JsonProperty("item")]
        public Item item { get; set; }

        [JsonProperty("request_id")]
        public string request_id { get; set; }

        [JsonProperty("error")]
        public PlaidResponseError Error { get; set; }

        [JsonProperty("has_error")]
        public bool HasError { get; set; }
    }

    public class Identity
    {
        public Address[] addresses { get; set; }
        public Email[] emails { get; set; }
        public string[] names { get; set; }
        public Phone_Numbers[] phone_numbers { get; set; }
    }

    public class Address
    {
        public string[] accounts { get; set; }
        public Data data { get; set; }
        public bool primary { get; set; }
    }

    public class Data
    {
        public string city { get; set; }
        public string state { get; set; }
        public string street { get; set; }
        public string zip { get; set; }
    }

    public class Email
    {
        public string data { get; set; }
        public bool primary { get; set; }
        public string type { get; set; }
    }

    public class Phone_Numbers
    {
        public string data { get; set; }
        public bool primary { get; set; }
        public string type { get; set; }
    }
}
