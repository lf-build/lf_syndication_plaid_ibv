﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
  public class PlaidProxy : IPlaidProxy
  {
    /// <summary>
    /// 
    /// </summary>
    private IPlaidConfiguration PlaidConfiguration { get; }

    /// <summary>
    /// client
    /// </summary>
    private IRestClient client { get; }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="plaidConfiguration"></param>
    public PlaidProxy(IPlaidConfiguration plaidConfiguration)
    {
      if (plaidConfiguration == null)
        throw new ArgumentNullException(nameof(plaidConfiguration));

      if (string.IsNullOrWhiteSpace(plaidConfiguration.BaseUrl))
        throw new ArgumentException("Plaid BaseURL Api Key is required", nameof(plaidConfiguration.BaseUrl));

      if (string.IsNullOrWhiteSpace(plaidConfiguration.ClientId))
        throw new ArgumentException("ClientId Api Key is required", nameof(plaidConfiguration.ClientId));

      if (string.IsNullOrWhiteSpace(plaidConfiguration.Secret))
        throw new ArgumentException("Secret Api Key is required", nameof(plaidConfiguration.Secret));

      PlaidConfiguration = plaidConfiguration;
      ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
      client = new RestClient(PlaidConfiguration.BaseUrl);
    }

    #region Plaid New sandbox

    /// <summary>
    /// PullTransactionAsync
    /// </summary>
    /// <param name="transactionRequest">transactionRequest</param>
    /// <returns>Transaction Response</returns>
    public async Task<IPlaidResponseAccounts> PullTransactionAsync(ITransactionRequest transactionRequestData, string institutionId)
    {
      IPlaidResponseAccounts response = new PlaidResponseAccounts();

      var transactionRequest = CreatingTransactionRequest(transactionRequestData);

      if (PlaidConfiguration.UseSimulation)
      {
        var simulationClient = new RestClient(PlaidConfiguration.SimulationURL);
        var request = new RestRequest("transactions/get", Method.POST);
        request.AddParameter("access_token", institutionId);

        var result = await ExecuteRequest<PlaidResponseAccounts>(simulationClient, request);

        if (result.Item2 == null)
        {
          response.Accounts = result.Item1.Accounts;
          response.Transactions = result.Item1.Transactions;
          foreach (var account in  transactionRequestData.SimulationAccounts)
          {
              var AccountType = account.AccountType;
              var accounts =  response.Accounts.Where(x =>x.SubType.ToLower() == AccountType.ToLower()).FirstOrDefault();
             var transactions = response.Transactions.Where(x =>x.PlaidAccountId == accounts.PlaidAccountId).ToList();
              foreach (var transaction in transactions)
              {
                  transaction.PlaidAccountId = account.SimulationAccountId;
              }
              accounts.PlaidAccountId = account.SimulationAccountId;              
          }
          
          response.Numbers = result.Item1.Numbers;
          response.Item = result.Item1.Item;
        }
        else
        {
          response.HasError = true;
          response.Error = result.Item2;
        }
      }
      else
      {
        var request = new RestRequest("transactions/get", Method.POST);
        request.JsonSerializer = new NewtonsoftJsonSerializer();
        request.AddJsonBody(transactionRequest);

        var result = await ExecuteRequest<PlaidResponseAccounts>(client, request);

        if (result.Item2 == null)
        {
          response.Transactions = result.Item1.Transactions;
          response.Accounts = result.Item1.Accounts;
          response.Numbers = result.Item1.Numbers;
          response.Item = result.Item1.Item;
        }
        else
        {
          response.HasError = true;
          response.Error = result.Item2;
        }
      }
      //response.error = result.Item2.ErrorMessage;
      //throw new PlaidException(result.Item2.ErrorCode, result.Item2.ErrorMessage);

      return response;
    }

    /// <summary>
    /// CreateItemAsync
    /// </summary>
    /// <param name="client"></param>
    /// <param name="requestData"></param>
    /// <returns></returns>
    public async Task<IPlaidResponsePublicTokenExchange> GetAccessTokenAsync(string public_token)
    {
      //// Exchange token
      var exchangeTokenRequest = new PlaidRequestPublicTokenExchange
      {
        Client_Id = PlaidConfiguration.ClientId,
        Public_Token = public_token,
        Secret = PlaidConfiguration.Secret
      };

      var request = new RestRequest("item/public_token/exchange", Method.POST);
      request.JsonSerializer = new NewtonsoftJsonSerializer();
      request.AddJsonBody(exchangeTokenRequest);

      var result = await ExecuteRequest<PlaidResponsePublicTokenExchange>(client, request);

      if (result.Item2 == null)
        return result.Item1;
      else
        throw new PlaidException(result.Item2.ErrorCode, result.Item2.ErrorMessage);
    }

    /// <summary>
    /// GetAccountAsync
    /// </summary>
    /// <param name="accessToken">accessToken</param>
    /// <returns>accounts data</returns>
    public async Task<IPlaidResponseAccounts> GetAccountAsync(string accessToken)
    {
      IPlaidResponseAccounts response = new PlaidResponseAccounts();
      var request = new RestRequest("accounts/get", Method.POST);
      request.JsonSerializer = new NewtonsoftJsonSerializer();
      var accountRequest = this.CreatingCommonModel(accessToken);
      request.AddJsonBody(accountRequest);
      var result = await ExecuteRequest<PlaidResponseAccounts>(client, request);

      if (result.Item2 == null)
      {
        response = result.Item1;
      }
      else
      {
        response.HasError = true;
        response.Error = result.Item2;
      }

      return response;
    }

    /// <summary>
    /// GetAccountAsync
    /// </summary>
    /// <param name="accessToken">accessToken</param>
    /// <returns>accounts data</returns>
    public async Task<IPlaidResponseAccounts> GetAccountsWithNumbersAsync(string accessToken, string insId = null)
    {
      IPlaidResponseAccounts response = new PlaidResponseAccounts();
      var request = new RestRequest("auth/get", Method.POST);
      request.JsonSerializer = new NewtonsoftJsonSerializer();
      var accountRequest = this.CreatingCommonModel(accessToken);
      request.AddJsonBody(accountRequest);

      if (PlaidConfiguration.UseSimulation)
      {
        var simulationClient = new RestClient(PlaidConfiguration.SimulationURL);
        request.AddParameter("access_token", insId);

        var result = await ExecuteRequest<PlaidResponseAccounts>(simulationClient, request);

        if (result.Item2 == null)
        {
          response = result.Item1;
          foreach (var account in response.Accounts)
          {
            var providerIdForSimulation = Guid.NewGuid().ToString("N");
            //var number = response.Numbers.FirstOrDefault(x =>x.Account_Id == account.PlaidAccountId);
            //number.Account_Id = providerIdForSimulation;
            account.PlaidAccountId = providerIdForSimulation;
          }
        }
        else
        {
          response.HasError = true;
          response.Error = result.Item2;
        }
      }
      else
      {

        var result = await ExecuteRequest<PlaidResponseAccounts>(client, request);

        if (result.Item2 == null)
        {
          response = result.Item1;
        }
        else
        {
          response.HasError = true;
          response.Error = result.Item2;
        }
      }

      return response;
    }

    /// <summary>
    /// Item Delete
    /// </summary>
    /// <param name="request">request body</param>
    /// <returns>boolean value</returns>
    public async Task<IPlaidResponseItemDelete> ItemDeleteAsync(string accessToken)
    {
      IPlaidResponseItemDelete returnResult = new PlaidResponseItemDelete();

      var request = new RestRequest("item/delete", Method.POST);
      request.JsonSerializer = new NewtonsoftJsonSerializer();
      request.AddJsonBody(new PlaidRequestItemDelete()
      {
        client_id = PlaidConfiguration.ClientId,
        secret = PlaidConfiguration.Secret,
        access_token = accessToken
      });

      var result = await ExecuteRequest<PlaidResponseItemDelete>(client, request);

      if (result.Item2 == null)
      {
        returnResult.Deleted = true;
        returnResult.AccessToken = accessToken;
        returnResult = result.Item1;
      }
      else
      {
        returnResult.Deleted = false;
        returnResult.AccessToken = accessToken;
        returnResult.error = result.Item2;
      }

      return returnResult;
    }

    /// <summary>
    /// GetPublicTokenAsync
    /// </summary>
    /// <param name="accessToken">accessToken</param>
    /// <returns>return publictoke</returns>
    public async Task<IPlaidCreatePublicTokenResponse> GetPublicTokenAsync(string accessToken)
    {
      var publicTokenRequest = this.CreatingCommonModel(accessToken);
      var request = new RestRequest("item/public_token/create", Method.POST);

      request.JsonSerializer = new NewtonsoftJsonSerializer();
      request.AddJsonBody(publicTokenRequest);

      var result = await ExecuteRequest<PlaidCreatePublicTokenResponse>(client, request);

      if (result.Item2 == null)
        return result.Item1;
      else
        throw new PlaidException(result.Item2.ErrorCode, result.Item2.ErrorMessage);
    }

    /// <summary>
    /// Get the InstitutionsGetByIdAsync
    /// </summary>
    /// <param name="institutionId">institutionId</param>
    /// <returns>institutions response</returns>
    public async Task<IPlaidResponseInstitution> InstitutionsGetByIdAsync(string institutionId)
    {
      IPlaidResponseInstitution response = new PlaidResponseInstitution();
      var institutionRequest = this.CreatingSearchByIdInstitutionModel(institutionId);
      var request = new RestRequest("institutions/get_by_id", Method.POST);
      request.JsonSerializer = new NewtonsoftJsonSerializer();
      request.AddJsonBody(institutionRequest);
      var result = await ExecuteRequest<PlaidResponseInstitution>(client, request);
      if (result.Item2 == null)
      {
        response = result.Item1;
      }
      else
      {
        response.HasError = true;
        response.Error = result.Item2;
      }

      return response;
    }

    /// <summary>
    /// GetIdentityAsync
    /// </summary>
    /// <param name="accessToken">accessToken</param>
    /// <returns>identity data</returns>
    public async Task<IPlaidResponseIdentity> GetIdentityAsync(string accessToken)
    {
      if (PlaidConfiguration.IsIdentityProduct)
      {
        IPlaidResponseIdentity response = new PlaidResponseIdentity();
        var request = new RestRequest("identity/get", Method.POST);
        request.JsonSerializer = new NewtonsoftJsonSerializer();
        var identityRequest = this.CreatingCommonModel(accessToken);
        request.AddJsonBody(identityRequest);

        var result = await ExecuteRequest<PlaidResponseIdentity>(client, request);
        if (result.Item2 == null)
        {
          response = result.Item1;
        }
        else
        {
          response.HasError = true;
          response.Error = result.Item2;
        }

        return response;
      }

      return null;
    }

    #endregion

    #region Private methods

    /// <summary>
    /// ExecuteRequest
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="client"></param>
    /// <param name="request"></param>
    /// <returns></returns>
    private async static Task<Tuple<T, PlaidResponseError>> ExecuteRequest<T>(IRestClient client, IRestRequest request)
    {
      return await Task.Run(async () =>
      {
        PlaidResponseError plaidResponseError = null;
        TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse>();

        RestRequestAsyncHandle handle = client.ExecuteAsync(request, restResponse => taskCompletion.SetResult(restResponse));
        IRestResponse responseResult = (IRestResponse)(await taskCompletion.Task);

        if (responseResult.ErrorException != null)
          throw new PlaidException("Exception occured while executing request", responseResult.ErrorException);

              //if (responseResult.ResponseStatus != ResponseStatus.Completed || responseResult.StatusCode != HttpStatusCode.OK)
              //    throw new PlaidException(
              //        $"Call failed. Status {responseResult.ResponseStatus} . Response: {responseResult.Content ?? ""}");

              if (responseResult.StatusCode == HttpStatusCode.BadRequest)
        {
          plaidResponseError = new PlaidResponseError();
          plaidResponseError = JsonConvert.DeserializeObject<PlaidResponseError>(responseResult.Content);
        }

        var result = JsonConvert.DeserializeObject<T>(responseResult.Content);

        Tuple<T, PlaidResponseError> tupleResult = new Tuple<T, PlaidResponseError>(result, plaidResponseError);

        return tupleResult;
      });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="accessToken">accessToken</param>
    /// <returns>common request model</returns>
    private IPlaidRequestCommon CreatingCommonModel(string accessToken)
    {
      return new PlaidRequestCommon
      {
        Access_Token = accessToken,
        Client_Id = this.PlaidConfiguration.ClientId,
        Secret = this.PlaidConfiguration.Secret
      };
    }

    private IPlaidRequestTransaction CreatingTransactionRequest(ITransactionRequest transactionRequestData)
    {
      return new PlaidRequestTransaction
      {
        AccessToken = transactionRequestData.AccessToken,
        ClientId = this.PlaidConfiguration.ClientId,
        Secret = this.PlaidConfiguration.Secret,
        EndDate = transactionRequestData.EndDate,
        StartDate = transactionRequestData.StartDate,
        Options = new OptionsForTransaction
        {
          Count = transactionRequestData.Count,
          Offset = transactionRequestData.Offset
        }
      };
    }

    /// <summary>
    /// CreatingInstitutionModel
    /// </summary>
    /// <returns>response</returns>
    private IPlaidRequestInstitutionById CreatingSearchByIdInstitutionModel(string institutionId)
    {
      var institutionRequest = new PlaidRequestInstitutionById
      {
        PublicKey = PlaidConfiguration.PublicKey,
        InstitutionId = institutionId
        //Options = new SearchOptions() { IncludeDisplayData = true }

      };
      return institutionRequest;
    }
    #endregion
  }
}
