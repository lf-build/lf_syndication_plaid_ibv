﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LendFoundry.Syndication.PlaidIBV {
    public interface IPlaidConfiguration : IDependencyConfiguration {
        string BaseUrl { get; set; }
        string ClientId { get; set; }
        string Secret { get; set; }
        bool UseSimulation { get; set; }
        string SimulationURL { get; set; }
        string PublicKey { get; set; }
        bool IsIdentityProduct { get; set; }

        string ClientName { get; set; }

        string ApiVersion { get; set; }

        string Env { get; set; }

        string Key { get; set; }

        List<string> Product { get; set; }

        bool SelectAccount { get; set; }

        string WebhookUrl { get; set; }
        string ConnectionString { get; set; }

    }
}