﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class Number
    {
        [JsonProperty("account")]
        public string AccountNumber { get; set; }

        [JsonProperty("account_id")]
        public string Account_Id { get; set; }

        [JsonProperty("routing")]
        public string Routing { get; set; }

        [JsonProperty("wire_routing")]
        public string Wire_Routing { get; set; }
    }
}
