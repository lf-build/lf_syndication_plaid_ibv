﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class Item
    {
        [JsonProperty("available_products")]
        public string[] AvailableProducts { get; set; }

        [JsonProperty("billed_products")]
        public string[] BilledProducts { get; set; }

        [JsonProperty("error")]
        public object Error { get; set; }

        [JsonProperty("institution_id")]
        public string Institution_Id { get; set; }

        [JsonProperty("item_id")]
        public string Item_Id { get; set; }

        [JsonProperty("webhook")]
        public string Webhook { get; set; }
    }
}
