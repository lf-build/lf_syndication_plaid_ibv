﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IAccounts
    {
        string PlaidAccountId { get; set; }

        string AccountName { get; set; }

        string AccountType { get; set; }

        string Mask { get; set; }

        Balances Balances { get; set; }

        bool IsLinked { get; set; }
    }

    public class Accounts : IAccounts
    {
        [JsonProperty("account_id")]
        public string PlaidAccountId { get; set; }

        [JsonProperty("name")]
        public string AccountName { get; set; }

        [JsonProperty("type")]
        public string AccountType { get; set; }

        [JsonProperty("mask")]
        public string Mask { get; set; }

        [JsonProperty("official_name")]
        public string OfficialName { get; set; }

        [JsonProperty("subtype")]
        public string SubType { get; set; }

        [JsonProperty("balances")]
        public Balances Balances { get; set; }

        public bool IsLinked { get; set; } = true;

        [JsonProperty("ownerName")]
        public string OwnerName { get; set; }
    }
}
