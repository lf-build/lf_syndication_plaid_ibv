﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class PaymentMeta
    {
        [JsonProperty("by_order_of")]
        public string ByOrderOf { get; set; }

        [JsonProperty("payee")]
        public string Payee { get; set; }

        [JsonProperty("payer")]
        public string Payer { get; set; }

        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }

        [JsonProperty("payment_processor")]
        public string PaymentProcessor { get; set; }

        [JsonProperty("ppd_id")]
        public string PpdId { get; set; }

        [JsonProperty("reason")]
        public string Reason { get; set; }

        [JsonProperty("reference_number")]
        public string ReferenceNumber { get; set; }
    }
}
