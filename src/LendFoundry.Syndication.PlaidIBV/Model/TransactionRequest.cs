﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface ITransactionRequest
    {
        string AccessToken { get; set; }

        string StartDate { get; set; }

        string EndDate { get; set; }

        int Count { get; set; }

        int Offset { get; set; }
        List<SimulationAccounts> SimulationAccounts{ get; set; }
    }
    public class TransactionRequest : ITransactionRequest
    {
        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }

        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }

        [JsonProperty("offset")]
        public int Offset { get; set; }

        public List<SimulationAccounts> SimulationAccounts{ get; set; }
    }

    public class SimulationAccounts 
    {
        public string SimulationAccountId { get; set; }
        public string AccountType { get; set; }

    }
}
