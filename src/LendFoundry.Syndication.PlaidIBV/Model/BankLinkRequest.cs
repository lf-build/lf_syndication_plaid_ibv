﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IBankLinkRequest
    {
        string PublicToken { get; set; }
        string AccessToken { get; set; }

    }
    public class BankLinkRequest : IBankLinkRequest
    {
        [JsonProperty("publicToken")]
        public string PublicToken { get; set; }

        [JsonProperty("accessToken")]
        public string AccessToken { get; set; }
    }
}
