﻿using Newtonsoft.Json;

namespace LendFoundry.Syndication.PlaidIBV
{
    public class Location
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("lat")]
        public string Lat { get; set; }

        [JsonProperty("lon")]
        public string Lon { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("store_number")]
        public string StoreNumber { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }
    }
}
