﻿using System.Collections.Generic;

namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidResponse
    {
        string PlaidItemId { get; set; }

        string InstitutionId { get; set; }

        string InstitutionName { get; set; }
        List<Accounts> Accounts { get; set; }

        List<Number> Numbers { get; set; }

        bool HasError { get; set; }

        List<Transaction> Transactions { get; set; }

        PlaidResponseError Error { get; set; }

        string AccessToken { get; set; }

        string[] Owners { get; set; }
    }

    public class PlaidResponse : IPlaidResponse
    {
        public PlaidResponse(IPlaidResponseAccounts response, string accessToken)
        {
            if (!response.HasError)
            {
                Accounts = response.Accounts;
                PlaidItemId = response.Item.Item_Id;
                Numbers = response.Numbers;
                Transactions = response.Transactions;
                InstitutionId = response.Item.Institution_Id;
                AccessToken = accessToken;
                InstitutionName = response.InstitutionName;
                Owners = response.Owners;
            }
            else
            {
                HasError = true;
                Error = response.Error;
            }
        }

        public PlaidResponse()
        {

        }

        public string PlaidItemId { get; set; }
         
        public string InstitutionId { get; set; }

        public string InstitutionName { get; set; }

        public List<Accounts> Accounts { get; set; }
         
        public List<Number> Numbers { get; set; }
        public bool HasError { get; set; }
         
        public List<Transaction> Transactions { get; set; }
         
        public PlaidResponseError Error { get; set; }

        public string AccessToken { get; set; }

        public string[] Owners { get; set; }
    }
}
