﻿using System.Threading.Tasks;
namespace LendFoundry.Syndication.PlaidIBV
{
    public interface IPlaidService
    {
        Task<IPlaidResponse> GetAccountsDetails(IBankLinkRequest bankLinkRequest, string insId = null);

        Task<IPlaidResponse> AccountSync(IBankLinkRequest bankLinkRequest);
        Task<IPlaidResponseItemDelete> DeleteItemConnection(IBankLinkRequest bankLinkRequest);

        Task<IPlaidResponse> AddBank(IBankLinkRequest bankLinkRequest);
        Task<IPlaidResponse> GetTransactions(ITransactionRequest request, string institutionId);

        Task<IPlaidCreatePublicTokenResponse> CreatePublicTokenForRefresh(IBankLinkRequest bankLinkRequest);
    }
}
