﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Syndication.PlaidIBV.Api
{
    /// <summary>
    /// Settings class
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// ServiceName
        /// </summary>
        /// <returns></returns>
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "plaidv2ibv";

    }
}
