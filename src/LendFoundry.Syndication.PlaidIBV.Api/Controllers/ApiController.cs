﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using System.Threading.Tasks;
using LendFoundry.EventHub;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif

namespace LendFoundry.Syndication.PlaidIBV.Api.Controllers
{
    /// <summary>
    /// ApiController
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="eventHubClient"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public ApiController(IPlaidService service, IEventHubClient eventHubClient, IPlaidConfiguration configuration,ILogger logger) : base(logger)
        {
            Service = service;
            EventHubClient = eventHubClient;
            Configuration = configuration; 
        }

        /// <summary>
        /// Gets Service
        /// </summary>
        private IPlaidService Service { get; }

        /// <summary>
        /// Gets EventHubClient
        /// </summary>
        private IEventHubClient EventHubClient { get; }

        /// <summary>
        /// Gets Configuration
        /// </summary>
        private IPlaidConfiguration Configuration { get; }


        /// <summary>
        /// GetAccountDetails
        /// </summary>
        /// <param name="request"></param>
        /// <param name="insId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/connect/item/{insId?}")]
#if DOTNET2
        [ProducesResponseType(typeof(IPlaidResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> GetAccountDetails([FromBody] BankLinkRequest request,string insId = null)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(request.PublicToken))
                        throw new InvalidArgumentException($"The {nameof(request.PublicToken)} cannot be null", nameof(request.PublicToken));

                    var accountDetails = await Service.GetAccountsDetails(request, insId);
                  
                    return Ok(accountDetails);
                }
                catch (PlaidException ex)
                {
                    Logger.Error($"The method GetAccountDetails({request.PublicToken}) raised an error: {ex}");
                    throw new NotFoundException($"Information not found with publictoken:{request.PublicToken}");
                }
            });
        }

        /// <summary>
        ///  AccountSync
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/account/sync")]
#if DOTNET2
        [ProducesResponseType(typeof(IPlaidResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> AccountSync([FromBody] BankLinkRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var accountDetails = await Service.AccountSync(request);

                    return Ok(accountDetails);
                }
                catch (PlaidException ex)
                {
                    Logger.Error($"The method AccountSync({request.AccessToken}) raised an error: {ex}");
                    throw new NotFoundException($"Information not found with AccessToken:{request.AccessToken}");
                }
            });
        }

        /// <summary>
        /// DeleteConnection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/delete/item")]
#if DOTNET2
        [ProducesResponseType(typeof(IPlaidResponseItemDelete), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public async Task<IActionResult> DeleteConnection([FromBody] BankLinkRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var ack = await Service.DeleteItemConnection(request);

                    return Ok(ack);
                }
                catch (PlaidException ex)
                {
                    Logger.Error($"The method DeleteConnection({request.PublicToken}) raised an error: {ex}");
                    throw new NotFoundException($"Information not found with publictoken:{request.PublicToken}");
                }
            });
        }

        /// <summary>
        /// DeleteConnection
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/refresh/plaidLink")]
        #if DOTNET2
        [ProducesResponseType(typeof(IPlaidResponseItemDelete), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        #endif
        public async Task<IActionResult> CreatePublicTokenForRefresh([FromBody] BankLinkRequest request)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    var result = await Service.CreatePublicTokenForRefresh(request);

                    return Ok(result);
                }
                catch (PlaidException ex)
                {
                    Logger.Error($"The method CreatePublicTokenForRefresh({request.PublicToken}) raised an error: {ex}");
                    throw new NotFoundException($"Information not found with publictoken:{request.PublicToken}");
                }
            });
        }


        #region PBGP Endpoints

        /// <summary>
        /// AddBank
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/addBank")]
#if DOTNET2
        [ProducesResponseType(typeof(IPlaidResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> AddBank([FromBody] BankLinkRequest request)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(request.AccessToken))
                        throw new InvalidArgumentException($"The {nameof(request.AccessToken)} cannot be null", nameof(request.AccessToken));

                    var response = await Service.AddBank(request);

                    return Ok(response);
                }
                catch (PlaidException ex)
                {
                    Logger.Error($"The method AddBank raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
 
        /// <summary>
        /// GetTransactions
        /// </summary>
        /// <param name="request"></param>
        /// <param name="institutionId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("/pullTransactions/{institutionId}")]
#if DOTNET2
        [ProducesResponseType(typeof(IPlaidResponse), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
#endif
        public Task<IActionResult> GetTransactions([FromBody] TransactionRequest request, string institutionId)
        {
            return ExecuteAsync(async () =>
            {
                try
                {
                    if (string.IsNullOrWhiteSpace(request.AccessToken))
                        throw new InvalidArgumentException($"The {nameof(request.AccessToken)} cannot be null", nameof(request.AccessToken));

                    var response = await Service.GetTransactions(request, institutionId);

                    return Ok(response);
                }
                catch (PlaidException ex)
                {
                    Logger.Error($"The method GetTransactions raised an error: {ex}");
                    return ErrorResult.BadRequest(ex.Message);
                }
            });
        }
        #endregion
    }
}
