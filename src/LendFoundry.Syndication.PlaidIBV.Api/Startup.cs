﻿using LendFoundry.Configuration.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Tenant.Client;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
namespace LendFoundry.Syndication.PlaidIBV.Api
{
    internal class Startup
    {
        public Startup(IHostingEnvironment env) { }

        public void ConfigureServices(IServiceCollection services)
        {
#if DOTNET2

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Probe"
                });
                c.AddSecurityDefinition("apiKey", new ApiKeyScheme()
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Syndication.PlaidIBV.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();

            // interface implements
            services.AddConfigurationService<PlaidConfiguration>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<PlaidConfiguration>(Settings.ServiceName);
            services.AddEventHub(Settings.ServiceName);
            services.AddTenantService();
            services.AddTransient<IPlaidConfiguration>(provider => provider.GetRequiredService<IConfigurationService<PlaidConfiguration>>().Get());
            services.AddTransient<IPlaidProxy, PlaidProxy>();
            services.AddTransient<IPlaidService, PlaidService>();

            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();
        }
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Tenant Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseHealthCheck();
            app.UseMvc();
        }
    }
}
