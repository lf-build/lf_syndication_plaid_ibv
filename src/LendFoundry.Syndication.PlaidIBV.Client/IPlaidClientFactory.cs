﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Syndication.PlaidIBV.Client
{
    public interface IPlaidClientFactory
    {
        IPlaidService Create(ITokenReader reader);
    }
}
