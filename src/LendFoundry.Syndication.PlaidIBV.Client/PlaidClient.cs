﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Threading.Tasks;

namespace LendFoundry.Syndication.PlaidIBV.Client
{
    public class PlaidClient : IPlaidService
    {
        public PlaidClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IPlaidResponse> GetAccountsDetails(IBankLinkRequest bankLinkRequest, string insId = null)
        {
            var request = new RestRequest($"/connect/item/{insId}", Method.POST);
            request.AddJsonBody(bankLinkRequest);
            return await Client.ExecuteAsync<PlaidResponse>(request);
        }

        public async Task<IPlaidResponse> AccountSync(IBankLinkRequest bankLinkRequest)
        {
            var request = new RestRequest("/account/sync", Method.POST);
            request.AddJsonBody(bankLinkRequest);
            return await Client.ExecuteAsync<PlaidResponse>(request);
        }

        public async Task<IPlaidResponseItemDelete> DeleteItemConnection(IBankLinkRequest bankLinkRequest)
        {
            var request = new RestRequest("/delete/item", Method.POST);
            request.AddJsonBody(bankLinkRequest);
            return await Client.ExecuteAsync<PlaidResponseItemDelete>(request);
        }

        public async Task<IPlaidResponse> AddBank(IBankLinkRequest bankLinkRequest)
        {
            var request = new RestRequest("/addBank", Method.POST);
            request.AddJsonBody(bankLinkRequest);
            return await Client.ExecuteAsync<PlaidResponse>(request);
        }

        public async Task<IPlaidResponse> GetTransactions(ITransactionRequest requestData, string institutionId)
        {
            var request = new RestRequest($"/pullTransactions/{institutionId}", Method.POST);
            request.AddJsonBody(requestData);
            return await Client.ExecuteAsync<PlaidResponse>(request);
        }

        public async Task<IPlaidCreatePublicTokenResponse> CreatePublicTokenForRefresh(IBankLinkRequest bankLinkRequest)
        {
            var request = new RestRequest("/refresh/plaidLink", Method.POST);
            request.AddJsonBody(bankLinkRequest);
            return await Client.ExecuteAsync<PlaidCreatePublicTokenResponse>(request);
        }
    }
}
