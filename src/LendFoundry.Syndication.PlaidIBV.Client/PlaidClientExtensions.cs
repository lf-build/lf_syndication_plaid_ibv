﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace LendFoundry.Syndication.PlaidIBV.Client
{
    public static class PlaidClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddPlaidIbv(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPlaidClientFactory>(p => new PlaidClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPlaidClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddPlaidIbv(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<IPlaidClientFactory>(p => new PlaidClientFactory(p, uri));
            services.AddSingleton(p => p.GetService<IPlaidClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPlaidIbv(this IServiceCollection services)
        {
            services.AddSingleton<IPlaidClientFactory>(p => new PlaidClientFactory(p));
            services.AddSingleton(p => p.GetService<IPlaidClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
